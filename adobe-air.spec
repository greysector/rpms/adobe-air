%define _enable_debug_packages %{nil}
%define debug_package          %{nil}

%define flash_ver 32.0.0.465

Summary: Cross-platform desktop applications runtime built using web technologies
Name: adobe-air
Version: 2.6.0
Release: 4
URL: https://www.adobe.com/devnet/air.html
# https://helpx.adobe.com/air/kb/archived-air-sdk-version.html
Source0: https://airdownload.adobe.com/air/lin/download/2.6/AdobeAIRSDK.tbz2
#Source0: https://airdownload.adobe.com/air/lin/download/2.6/AdobeAIRInstaller.bin
Source1: https://helpx.adobe.com/air/release-note/release-notes-developer-air-2-1.html
Source2: https://aur.archlinux.org/cgit/aur.git/plain/adobe-air?h=adobe-air#/adobe-air
Source3: https://archive.org/download/all-flash-V32-installers/linux-32bit-32-npapi-flash_player_npapi_linux.i386.tar.gz
License: Adobe
ExclusiveArch: i686
# https://helpx.adobe.com/air/kb/packages-required-run-air-2.html
# optional: libkwallet.so* libkwalletclient.so* libkdecore.so*
Recommends: alsa-plugins-pulseaudio%{_isa}
Recommends: libDCOP.so.4
Recommends: libkdecore.so.4
Recommends: libkwalletclient.so.1
Recommends: libqt-mt.so.3
BuildRequires: chrpath
Requires: ca-certificates
Requires: gtk2-engines%{_isa}
Requires: libcurl.so.4
Requires: xdg-utils
Provides: bundled(flash-player) = %{flash_ver}

%global __provides_exclude_from ^%{_libdir}/%{name}
%global __requires_exclude      ^lib\(c\\.so\\.6\\(GLIBC_PRIVATE\\)\|DCOP\\.so\\.4\|hal\\.so\\.1\|kdecore\\.so\\.4\|kwalletclient\\.so\\.1\|qt-mt\\.so\\.3\)\|lib\(curl\|eggtray\|pacparser\)\\.so$

%description
Adobe AIR is a cross-platform runtime that enables you to use your existing
Flash/ActionScript or HTML/JavaScript development skills and tools to build and
deploy applications, games, and videos outside the browser and on mobile
devices.

%prep
%setup -qc -a 3
chmod +x libflashplayer.so
mv libflashplayer.so "runtimes/air/linux/Adobe AIR/Versions/1.0/Resources/"
pushd "runtimes/air/linux/Adobe AIR/Versions/1.0/Resources"
rm \
  curl-ca-bundle.crt \
  libcurl.so \
  setup.deb \

rm -r \
  appinstall \
  nss3 \
  xdg-utils \

chrpath -r '$ORIGIN:$ORIGIN/Resources' \
  libadobecp15.so \
  libpacparser.so \

ln -s /usr/lib/libcurl.so.4 libcurl.so
popd
cp -p %{S:1} ./

%build

%install
install -dm755 %{buildroot}%{_libdir}/%{name}/{bin,runtimes/air/linux}
install -pm755 bin/adl %{buildroot}%{_libdir}/%{name}/bin/
cp -pr runtimes/air/linux %{buildroot}%{_libdir}/%{name}/runtimes/air/
ln -s /etc/pki/tls/certs/ca-bundle.crt "%{buildroot}%{_libdir}/%{name}/runtimes/air/linux/Adobe AIR/Versions/1.0/Resources/curl-ca-bundle.crt"

%files
%doc "AIR SDK Readme.txt" release-notes-developer-air-2-1.html
%license "AIR SDK license.pdf"
%{_libdir}/%{name}

%changelog
* Thu Jun 27 2024 Dominik Mierzejewski <dominik@greysector.net> 2.6.0-4
- make kdelibs3 and qt3 dependency weak
- use ExclusiveArch instead of BuildArch to fix srpm build

* Fri Jun 02 2023 Dominik Mierzejewski <dominik@greysector.net> 2.6.0-3
- original Flash tarball unavailable, switch to archived mirror

* Tue Apr 14 2020 Dominik Mierzejewski <rpm@greysector.net> 2.6.0-2
- unbundle curl

* Wed Nov 21 2018 Dominik Mierzejewski <rpm@greysector.net> 2.6.0-1
- initial build
